using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day25
    {
        public static void Run()
        {
            var sourceCode = Misc.ReadInput(day: 25);
            var assemblyProgram = new MoreUpdatedAssemblyProgram(sourceCode);

            // Part 1
            var registerA = RegisterAValueThatProducesClockSignal(assemblyProgram);
            Console.WriteLine("Clock signal is produced when A is: {0}", registerA);
        }

        private static int RegisterAValueThatProducesClockSignal(MoreUpdatedAssemblyProgram program)
        {
            for (var a = 0;; ++a)
            {
                program.A.Value = a;
                try
                {
                    program.Execute();
                }
                catch (MoreUpdatedAssemblyProgram.IsClockSignalException)
                {
                    return a;
                }
                catch (MoreUpdatedAssemblyProgram.NotClockSignalException)
                {
                    program.Reset();
                    
                }
            }
        }
    }

    public class MoreUpdatedAssemblyProgram : UpdatedAssemblyProgram
    {
        private static int _endTransmissionAfterElements = 100;

        public MoreUpdatedAssemblyProgram(IEnumerable<string> instructions) : base(instructions) {}

        private IList<int> Transmission { get; } = new List<int>();

        public void Reset()
        {
            A.Value = 0;
            B.Value = 0;
            C.Value = 0;
            D.Value = 0;
            InstructionPointer = 0;
            Transmission.Clear();
        }

        protected override Instruction ParseInstruction(string code)
        {
            var splits = code.Split();
            switch (splits[0])
            {
                case "out":
                    return new Transmit(GetValueSource(splits[1]), Transmission);
                default:
                    return base.ParseInstruction(code);
            }
        }

        public class Transmit : Instruction
        {
            public Transmit(ValueSource valueSource, IList<int> transmission)
            {
                ValueSource = valueSource;
                Transmission = transmission;
            }

            public ValueSource ValueSource { get; }

            public IList<int> Transmission { get; }

            public int Execute()
            {
                if (Transmission.Count() % 2 != ValueSource.Value)
                    throw new NotClockSignalException();
                Transmission.Add(ValueSource.Value);
                if (Transmission.Count() >= _endTransmissionAfterElements)
                    throw new IsClockSignalException();
                return 1;
            }
        }

        [System.Serializable]
        public class IsClockSignalException : System.Exception
        {
            public IsClockSignalException() { }
            public IsClockSignalException( string message ) : base( message ) { }
            public IsClockSignalException( string message, System.Exception inner ) : base( message, inner ) { }
            protected IsClockSignalException(
                System.Runtime.Serialization.SerializationInfo info,
                System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { }
        }

        [System.Serializable]
        public class NotClockSignalException : System.Exception
        {
            public NotClockSignalException() { }
            public NotClockSignalException( string message ) : base( message ) { }
            public NotClockSignalException( string message, System.Exception inner ) : base( message, inner ) { }
            protected NotClockSignalException(
                System.Runtime.Serialization.SerializationInfo info,
                System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { }
        }
    }
}