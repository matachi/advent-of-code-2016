using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day23
    {
        public static void Run()
        {
            var sourceCode = Misc.ReadInput(day: 23);

            // Part 1
            var assemblyProgram = new UpdatedAssemblyProgram(sourceCode);
            assemblyProgram.A.Value = 7;
            assemblyProgram.Execute();
            Console.WriteLine("Value in register A: {0}", assemblyProgram.A.Value);

            // Part 2
            assemblyProgram = new UpdatedAssemblyProgram(OptimizeMultiplication(sourceCode, 4));
            assemblyProgram.A.Value = 12;
            assemblyProgram.Execute();
            Console.WriteLine("Value in register A: {0}", assemblyProgram.A.Value);
        }

        /// <summary>Rudimentary opimization where multiplication is made more efficient</summary>
        /// <remarks>
        /// Replaces:
        ///     cpy b c
        ///     inc a
        ///     dec c
        ///     jnz c -2
        ///     dec d
        ///     jnz d -5
        /// With:
        ///     mul d b
        ///     add a d
        ///     cpy 0 c
        ///     cpy 0 d
        ///     nop
        ///     nop
        /// </remarks>
        private static IEnumerable<string> OptimizeMultiplication(string[] sourceCode, int line)
        {
            var list = sourceCode.ToList();
            list.RemoveRange(line, 6);
            list.InsertRange(line, new []{ "mul d b", "add a d", "cpy 0 c", "cpy 0 d", "nop", "nop" });
            return list.ToArray();
        }
    }


    public class UpdatedAssemblyProgram : AssemblyProgram
    {
        public UpdatedAssemblyProgram(IEnumerable<string> instructions) : base(instructions) {}

        protected override Instruction ParseInstruction(string code)
        {
            var splits = code.Split();
            switch (splits[0])
            {
                case "tgl":
                    return new Toggle(GetValueSource(splits[1]), Instructions);
                case "add":
                    return new Add(GetValueSource(splits[1]), GetValueSource(splits[2]));
                case "mul":
                    return new Multiply(GetValueSource(splits[1]), GetValueSource(splits[2]));
                case "nop":
                    return new NoOperation();
                default:
                    return base.ParseInstruction(code);
            }
        }

        public class Toggle : Instruction
        {
            public Toggle(ValueSource valueSource, IList<Instruction> instructions)
            {
                ValueSource = valueSource;
                Instructions = instructions;
            }

            private ValueSource ValueSource { get; }

            private IList<Instruction> Instructions { get; }

            public int Execute()
            {
                var indexOfThis = Instructions.IndexOf(this);
                var indexToToggle = indexOfThis + ValueSource.Value;

                // Nothing happens if it's outside the program
                if (indexToToggle < 0 || indexToToggle >= Instructions.Count())
                    return 1;

                // Toggle the instruction
                var instructionToToggle = Instructions[indexToToggle];
                Instruction newInstruction;
                switch (instructionToToggle)
                {
                    case Increment i:
                        newInstruction = new Decrement(i.ValueSource);
                        break;
                    case Decrement i:
                        newInstruction = new Increment(i.ValueSource);
                        break;
                    case JumpIfNotZero i:
                        newInstruction = new Copy(i.ValueSource, i.Steps);
                        break;
                    case Copy i:
                        newInstruction = new JumpIfNotZero(i.From, i.To);
                        break;
                    case Toggle i:
                        newInstruction = new Increment(i.ValueSource);
                        break;
                    default:
                        throw new ArgumentException("Unsupported instruction.");
                }
                Instructions[indexToToggle] = newInstruction;

                return 1;
            }
        }

        public class Add : Instruction
        {
            public Add(ValueSource to, ValueSource from)
            {
                To = to;
                From = from;
            }

            public ValueSource To { get; }

            public ValueSource From { get; }

            public int Execute()
            {
                if (To is Register r)
                {
                    r.Value += From.Value;
                }
                return 1;
            }
        }

        public class Multiply : Instruction
        {
            public Multiply(ValueSource to, ValueSource from)
            {
                To = to;
                From = from;
            }

            public ValueSource To { get; }

            public ValueSource From { get; }

            public int Execute()
            {
                if (To is Register r)
                {
                    r.Value *= From.Value;
                }
                return 1;
            }
        }

        public class NoOperation : Instruction
        {
            public int Execute()
            {
                return 1;
            }
        }
    }
}