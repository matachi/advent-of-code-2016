# Advent of Code 2016

Solutions implemented in C#.

Author: Daniel Jonsson  
License: [MIT License](LICENSE)

## Favorite problems and solutions

### Day 13 & 24: A*

I'm most proud of my solution to [day 13](Day13.cs). In it, I did a C# implementation of the
[A* algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm). It uses C# techniques such as
[yield](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/yield),
[extension methods](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/extension-methods),
[LINQ](https://msdn.microsoft.com/en-us/library/bb308959.aspx),
[custom methods for LINQ queries](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/how-to-add-custom-methods-for-linq-queries),
[named arguments](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/named-and-optional-arguments),
[multidimensional arrays](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/arrays/multidimensional-arrays),
[object-oriented programming](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/object-oriented-programming),
[auto-implemented properties](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/auto-implemented-properties),
[generic methods](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/generics/generic-methods),
[C# 7.0 tuple syntax](https://blogs.msdn.microsoft.com/dotnet/2016/08/24/whats-new-in-csharp-7-0/),
[C# 7.0 type casting with pattern matching](https://msdn.microsoft.com/en-us/magazine/mt790184.aspx),
[XML documentation](https://msdn.microsoft.com/en-us/library/aa288481(v=vs.71).aspx) and
[array initializer](https://msdn.microsoft.com/en-us/library/aa664573(v=vs.71).aspx). Furthermore, it also has a unit
test in [Tests/Day13.cs](Tests/Day13.cs), which uses the input and expected output from the example in the [task
description](https://msdn.microsoft.com/en-us/library/hh694602.aspx).

The solution to day 13 I reused for [day 24](Day24.cs), where I subclassed the
2D-grid class, containing the A* algorithm, and added some additional functions
to solve the day's problem, which was a [travelling salesman
problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem).

### Day 11, 23 & 25: Assembly virtual machine

Next on my list of problems I liked is [day 12](Day12.cs), where the goal was
to interpret and run an assembly-like language and read the value from one of
the registers when finished.

The instruction set it had to support was (1) increment the value in one of the
registers, (2) decrement the value in one of the registers, (3) copy a constant
or a register's value to a register and (4) jump if a constant or register is
not zero.

I modeled the constants and registers as classes, inheriting from a shared
interface called `ValueSource`. Each instruction I also modeled as its own
class, inheriting from an interface called `Instruction` that contained a
function `Execute` that would perform the instruction's logic and return the
number of steps the instruction pointer should be incremented with. With this
approach, I managed to make the logic very simple and readable, with very short
and succint methods, and it is also very extensible.

The solution from day 11 I later reused for [day 23](Day23.cs). There, I
subclassed the `AssemblyProgram` class and added support for four additional
instructions. One of the instructions was a toggle instruction, that modifies
another instruction in the source code during runtime according to specified
rules. I also added instructions for performing addition and multiplication to
speed up a rudimentary loop that performed multiplication.

During [day25](Day25.cs), the assembly virtual machine was extended once again.
This time, it had to support an instruction that would transmit a number out
from the program during runtime. During the program's execution, the transmit
instruction was executed repeatadly, and the goal was to find the initial value
in register A that would make it transmit a sequence looking like "0, 1, 0, 1,
0..." To make this efficent, I made it so that the transmit instruction would
halt the program by throwing an exception as soon as it detects that the wrong
sequence is being sent. Once again, the actual "business" logic is very simple,
and most of the code is just boilerplate things.

## How to run

Run:

    $ dotnet run 13

Where the number may be the number of any solved day.

## How to run the unit tests

Run all tests:

    $ dotnet test

Run a single test:

    $ dotnet test --filter DisplayName=AdventOfCode2016.Tests.Day14Tests.Part1Test

Run some tests:

    $ dotnet test --filter DisplayName~AdventOfCode2016.Tests.Day17Tests
