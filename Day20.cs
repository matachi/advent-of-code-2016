using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day20
    {
        public static void Run()
        {
            var blockedIps = Misc.ReadInput(day: 20);

            // Part 1
            var firstAllowedIpAddress = FirstAllowedIpAddress(blockedIps);
            Console.WriteLine("First allowed IP address: {0}", firstAllowedIpAddress);

            // Part 2
            var totalNumberOfAllowedIpAddresses = TotalNumberOfAllowedIpAddresses(blockedIps);
            Console.WriteLine("Total number of allowed IP addresses: {0}", totalNumberOfAllowedIpAddresses);
        }

        private static uint FirstAllowedIpAddress(string[] blockedRanges)
        {
            return BlockedRanges(blockedRanges).First().End+1;
        }

        private static uint TotalNumberOfAllowedIpAddresses(string[] blockedRanges)
        {
            var ranges = BlockedRanges(blockedRanges);
            return Enumerable
                .Range(0, ranges.Count() - 1)
                .Select(i => ranges[i+1].Start-ranges[i].End-1)
                .Aggregate((i, j) => i + j);
        }

        private static IList<(uint Start, uint End)> BlockedRanges(string[] blockedRanges)
        {
            var ranges = new List<(uint Start, uint End)>();
            foreach (var range in blockedRanges)
            {
                var splits = range.Split('-');
                var start = uint.Parse(splits.First());
                var end = uint.Parse(splits.Last());
                ranges.Add((start, end));
            }
            ranges.Sort((i, j) => i.Start.CompareTo(j.Start));
            for (var i = 0; i < ranges.Count(); ++i)
            {
                var range = ranges[i];
                for (var j = i+1; j < ranges.Count();)
                {
                    var secondRange = ranges[j];
                    if ((range.Start >= secondRange.Start && range.Start <= secondRange.End+1) ||
                        (range.End >= secondRange.Start-1 && range.End <= secondRange.End) ||
                        (range.Start <= secondRange.Start && range.End >= secondRange.End) ||
                        (range.Start >= secondRange.Start && range.End <= secondRange.End))
                    {
                        range = (Math.Min(range.Start, secondRange.Start), Math.Max(range.End, secondRange.End));
                        ranges[i] = range;
                        ranges.RemoveAt(j);
                    }
                    else
                        ++j;
                }
            }
            return ranges;
        }
    }
}