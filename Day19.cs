using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day19
    {
        public static void Run()
        {
            var puzzleInput = 3014387;

            // Part 1
            var elfThatGetsAllPresents = ElfThatGetsAllPresents(puzzleInput);
            Console.WriteLine("Elf that gets all presents: {0}", elfThatGetsAllPresents);

            // Part 2
            elfThatGetsAllPresents = ElfThatGetsAllPresents2(puzzleInput);
            Console.WriteLine("Elf that gets all presents: {0}", elfThatGetsAllPresents);
        }

        public static int ElfThatGetsAllPresents(int numberOfElves)
        {
            var elf = InitElves(numberOfElves);
            while (elf.Next != elf)
            {
                var nextElf = elf.Next;
                var nextNextElf = nextElf.Next;
                elf.Next = nextNextElf;
                elf = nextNextElf;
            }
            return elf.Number;
        }

        public static int ElfThatGetsAllPresents2(int numberOfElves)
        {
            var elf = InitElves(numberOfElves);
            for (var i = 0; i < numberOfElves / 2 - 1; ++i)
                elf = elf.Next;
            var removeOnce = numberOfElves % 2 == 1;
            while (elf.Next != elf)
            {
                var nextElf = elf.Next;
                var nextNextElf = nextElf.Next;
                var nextNextNextElf = nextNextElf.Next;
                if (removeOnce || nextElf == nextNextNextElf)
                {
                    removeOnce = false;
                    elf.Next = nextNextElf;
                    elf = nextNextElf;
                }
                else
                {
                    elf.Next = nextNextNextElf;
                    elf = nextNextNextElf;
                }
            }
            return elf.Number;
        }

        private static Elf InitElves(int numberOfElves)
        {
            var firstElf = new Elf{ Number = 1 };
            var previousElf = firstElf;
            foreach (var i in Enumerable.Range(2, numberOfElves - 1))
            {
                var newElf = new Elf{ Number = i };
                previousElf.Next = newElf;
                previousElf = newElf;
            }
            previousElf.Next = firstElf;
            return firstElf;
        }

        private class Elf
        {
            public int Number { get; set; }
            public Elf Next { get; set; }
        }
    }
}