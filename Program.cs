using System;
using System.Reflection;

namespace AdventOfCode2016
{
    public class Program
    {
        public static void Main(string[] args)
        {
           var type = Type.GetType($"AdventOfCode2016.Day{args[0]}");
           var method = type.GetMethod("Run", BindingFlags.Static | BindingFlags.Public);
           method?.Invoke(null, null);
        }
    }
}
