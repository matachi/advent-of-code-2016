using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day22
    {
        public static void Run()
        {
            var dfOutput = Misc.ReadInput(day: 22);

            // Part 1
            var numberOfViablePairs = ViablePairs(dfOutput).Count();
            Console.WriteLine("Number of viable pairs: {0}", numberOfViablePairs);

            // Part 2
            // Look at the printed grid. Moving the empty space to the top row requires 28 moves. Moving it to the
            // top-right requires 32 moves. This does also move our data of interest 1 step to the left. We need to
            // move the data 32 additional steps, each requiring 5 moves. The total sum of moves therefore is
            // 28 + 32 + 5 * 32 = 220.
            PrintGrid(dfOutput);
        }

        private static IEnumerable<(Node, Node)> ViablePairs(string[] dfOutput)
        {
            var grid = InitGrid(dfOutput);
            var pairs = GetAllPairs(grid);
            var viablePairs = pairs.Where(p => IsViable(p));
            return viablePairs;
        }

        private static void PrintGrid(string[] dfOutput)
        {
            var grid = InitGrid(dfOutput);

            for (var y = 0; y < grid.GetLength(1); ++y)
            {
                for (var x = 0; x < grid.GetLength(0); ++x)
                {
                    var node = grid[x, y];
                    var symbol = ".";
                    if (node.Used == 0)
                        symbol = "_";
                    if (node.Used > grid[33, 0].Used)
                        symbol = "#";
                    Console.Write(symbol);
                }
                Console.WriteLine();
            }
        }

        private static Node[,] InitGrid(string[] dfOutput)
        {
            var grid = new Node[34, 30];
            var row = 2;
            for (var x = 0; x < grid.GetLength(0); ++x)
            {
                for (var y = 0; y < grid.GetLength(1); ++y)
                {
                    var splits = dfOutput[row].Split().Where(s => !string.IsNullOrEmpty(s)).ToList();
                    var node = new Node{
                        Size=splits[1].Substring(0, splits[1].Count()-1).ToInt(),
                        Used=splits[2].Substring(0, splits[2].Count()-1).ToInt()
                    };
                    grid[x, y] = node;
                    ++row;
                }
            }
            return grid;
        }

        private static IEnumerable<(Node, Node)> GetAllPairs(Node[,] grid)
        {
            var nodes = new List<Node>();
            for (var x = 0; x < grid.GetLength(0); ++x)
            {
                for (var y = 0; y < grid.GetLength(1); ++y)
                {
                    nodes.Add(grid[x, y]);
                }
            }
            for (var i = 0; i < nodes.Count(); ++i)
            {
                for (var j = i+1; j < nodes.Count(); ++j)
                {
                    yield return (nodes[i], nodes[j]);
                    yield return (nodes[j], nodes[i]);
                }
            }
        }

        private static bool IsViable((Node A, Node B) pair)
        {
            if (pair.A.Used == 0)
                return false;
            return pair.A.Used <= pair.B.Available;
        }

        private class Node
        {
            public int Size { get; set; }
            public int Used { get; set; }
            public int Available => Size - Used;
        }
    }
}