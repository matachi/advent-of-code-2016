using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day9
    {
        public static void Run()
        {
            var compressedInput = Misc.ReadInput(day: 9)[0];
            var decompressedLength1 = DecompressedLengthVersion1(compressedInput);
            Console.WriteLine("Version 1 decompressed length: {0}", decompressedLength1);
            var decompressedLength2 = DecompressedLengthVersion2(compressedInput);
            Console.WriteLine("Version 2 decompressed length: {0}", decompressedLength2);
        }

        public static long DecompressedLengthVersion1(string input)
        {
            var length = 0;
            var i = 0;
            while (i < input.Count())
            {
                if (input[i] == '(')
                {
                    var end = input.IndexOf(')', i);
                    var command = input.Substring(i + 1, end - i - 1).Split('x').ToArray();
                    var numberOfCharacters = int.Parse(command[0]);
                    var times = int.Parse(command[1]);
                    length += times * numberOfCharacters;
                    i = end + numberOfCharacters + 1;
                }
                else
                {
                    ++i;
                    ++length;
                }
            }
            return length;
        }

        public static long DecompressedLengthVersion2(string input)
        {
            return DecompressedLengthVersion2(input, 0, input.Count() - 1);
        }

        private static long DecompressedLengthVersion2(string input, int start, int stop)
        {
            long length = 0;
            var i = start;
            while (i <= stop)
            {
                if (input[i] == '(')
                {
                    var end = input.IndexOf(')', i);
                    var command = input.Substring(i + 1, end - i - 1).Split('x').ToArray();
                    var numberOfCharacters = int.Parse(command[0]);
                    var times = int.Parse(command[1]);
                    length += times * DecompressedLengthVersion2(input, end + 1, end + numberOfCharacters);
                    i = end + numberOfCharacters + 1;
                }
                else
                {
                    ++i;
                    ++length;
                }
            }
            return length;
        }
    }
}