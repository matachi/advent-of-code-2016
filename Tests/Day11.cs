using System.Collections.Generic;
using Xunit;

namespace AdventOfCode2016.Tests
{
    public class Day11Tests
    {
        [Fact]
        public void Part1Test()
        {
            var configuration = new Day11.Configuration();
            configuration.Add(new List<int>());
            configuration.Add(new List<int>());
            configuration.Add(new List<int>());
            configuration.Add(new List<int>());
            configuration[0].Add((int) Rock.Cobalt);
            configuration[0].Add((int) Rock.Polonium);
            configuration[1].Add(-(int) Rock.Cobalt);
            configuration[2].Add(-(int) Rock.Polonium);

            Assert.Equal(9, Day11.FindMinimumNumberOfSteps(configuration));
        }
    }
}