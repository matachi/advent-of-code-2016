using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AdventOfCode2016.Tests
{
    public class Day19Tests
    {
        [Theory]
        [InlineData(5, 3)]
        public void Part1Test(int numberOfElves, int elfThatGetsAllPresents)
        {
            Assert.Equal(elfThatGetsAllPresents, Day19.ElfThatGetsAllPresents(numberOfElves));
        }

        [Theory]
        [InlineData(5, 2)]
        public void Part2Test(int numberOfElves, int elfThatGetsAllPresents)
        {
            Assert.Equal(elfThatGetsAllPresents, Day19.ElfThatGetsAllPresents2(numberOfElves));
        }
    }
}