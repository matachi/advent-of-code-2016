using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AdventOfCode2016.Tests
{
    public class Day18Tests
    {
        [Theory]
        [InlineData("..^^.", 3, 6)]
        [InlineData(".^^.^.^^^^", 10, 38)]
        public void Part1Test(string initialRow, int rows, int safeTiles)
        {
            Assert.Equal(safeTiles, Day18.NumberOfSafeTiles(initialRow, rows));
        }
    }
}