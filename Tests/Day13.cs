using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AdventOfCode2016.Tests
{
    public class Day13Tests
    {
        [Fact]
        public void Part1Test()
        {
            // Arrange
            var board = new Day13.Board(10);

            // Act
            var shortestPath = board.ShortestPath((1, 1), (7, 4));
            var numberOfSteps = shortestPath.Count() - 1;

            // Assert
            Assert.Equal(11, numberOfSteps);
        }
    }
}