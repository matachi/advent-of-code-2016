using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AdventOfCode2016.Tests
{
    public class Day14Tests
    {
        [Fact]
        public void Part1Test()
        {
            // Arrange
            var salt = "abc";

            // Act
            var keyIndex = Day14.FindIndexOfKey(salt);

            // Assert
            Assert.Equal(22728, keyIndex);
        }

        [Fact]
        public void Part2Test()
        {
            // Arrange
            var salt = "abc";

            // Act
            var keyIndex = Day14.FindIndexOfKey(salt, 2016);

            // Assert
            Assert.Equal(22551, keyIndex);
        }
    }
}