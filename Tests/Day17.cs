using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AdventOfCode2016.Tests
{
    public class Day17Tests
    {
        [Theory]
        [InlineData("ihgpwlah", "DDRRRD")]
        [InlineData("kglvqrro", "DDUDRLRRUDRD")]
        [InlineData("ulqzkmiv", "DRURDRUDDLLDLUURRDULRLDUUDDDRR")]
        public void Part1Test(string passcode, string path)
        {
            Assert.Equal(path, Day17.FindShortestPathToExit(passcode));
        }

        [Theory]
        [InlineData("ihgpwlah", 370)]
        [InlineData("kglvqrro", 492)]
        [InlineData("ulqzkmiv", 830)]
        public void Part2Test(string passcode, int length)
        {
            Assert.Equal(length, Day17.FindLongestPathLengthToExit(passcode));
        }
    }
}