using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AdventOfCode2016.Tests
{
    public class Day24Tests
    {
        [Fact]
        public void Part1Test()
        {
            // Arrange
            var board = new Day24.UpdatedBoard(rows: new string[] {
                "###########",
                "#0.1.....2#",
                "#.#######.#",
                "#4.......3#",
                "###########"
            });

            // Act
            var shortestRouteLength = board.ShortestRouteLength();

            // Assert
            Assert.Equal(14, shortestRouteLength);
        }

        [Theory]
        [InlineData(2, 4, 32)]
        public void ShortestPath(int start, int end, int steps)
        {
            // Arrange
            var rows = Misc.ReadInputFromUnittest(24);
            var board = new Day24.UpdatedBoard(rows);
            var startCoordinate = CoordinateOfNumber(rows, start);
            var endCoordinate = CoordinateOfNumber(rows, end);

            // Act
            var shortestPathLength = board.ShortestPath(startCoordinate, endCoordinate).Count()-1;

            // Assert
            Assert.Equal(steps, shortestPathLength);
        }

        private (int X, int Y) CoordinateOfNumber(string[] rows, int number)
        {
            for (var y = 0; y < rows.Count(); ++y)
            {
                for (var x = 0; x < rows[y].Count(); ++x)
                {
                    if (rows[y][x] == $"{number}"[0])
                        return (x, y);
                }
            }
            throw new System.ArgumentException("Cannot find the number in the grid.");
        }
    }
}