using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day2
    {
        public static void Run()
        {
            var input = Misc.ReadInput(day: 2);
            var code = input.Aggregate("5",
                (partialCode, instruction) => partialCode + ExtractNumberFromInstruction(partialCode.Last(), instruction),
                (partialCode) => partialCode.Skip(1).Select(chr => chr.ToString()).Aggregate((s1, s2) => s1 + s2));
            Console.WriteLine("First code: {0}", code);

            var code2 = input.Aggregate("5",
                (partialCode, instruction) => partialCode + ExtractNumberFromInstruction2(partialCode.Last(), instruction),
                (partialCode) => partialCode.Skip(1).Select(chr => chr.ToString()).Aggregate((s1, s2) => s1 + s2));
            Console.WriteLine("Second code: {0}", code2);
        }

        /// Keypad:
        /// 1 2 3
        /// 4 5 6
        /// 7 8 9
        private static char ExtractNumberFromInstruction(char startAt, string instruction)
        {
            var currentKey = startAt;
            foreach (var direction in instruction)
            {
                if (direction == 'U' && currentKey > '3')
                {
                    currentKey -= (char) 3;
                }
                else if (direction == 'D' && currentKey < '7')
                {
                    currentKey += (char) 3;
                }
                else if (direction == 'L' && ((currentKey - '1') % 3) != 0)
                {
                    currentKey -= (char) 1;
                }
                else if (direction == 'R' && ((currentKey - '1') % 3) != 2)
                {
                    currentKey += (char) 1;
                }
            }
            return currentKey;
        }

        /// Keypad:
        ///     1
        ///   2 3 4
        /// 5 6 7 8 9
        ///   A B C
        ///     D
        private static char ExtractNumberFromInstruction2(char startAt, string instruction)
        {
            var currentCoordinate = SecondKeypadCoordinate(startAt);
            foreach (var direction in instruction)
            {
                Coordinate nextCoordinate = currentCoordinate;
                if (direction == 'U')
                {
                    nextCoordinate += new Coordinate{X = 0, Y = -1};
                }
                else if (direction == 'D')
                {
                    nextCoordinate += new Coordinate{X = 0, Y = 1};
                }
                else if (direction == 'L')
                {
                    nextCoordinate += new Coordinate{X = -1, Y = 0};
                }
                else if (direction == 'R')
                {
                    nextCoordinate += new Coordinate{X = 1, Y = 0};
                }
                else
                {
                    throw new ArgumentException("Invalid direction");
                }
                if (SecondKeypad[nextCoordinate.Y][nextCoordinate.X] != ' ')
                {
                    currentCoordinate = nextCoordinate;
                }
            }
            return SecondKeypad[currentCoordinate.Y][currentCoordinate.X];
        }

        private static Coordinate SecondKeypadCoordinate(char position)
        {
            return SecondKeypad
                .SelectMany((row, y) => row
                    .Select((c, x) => new {Char = c, Coordinate = new Coordinate{X = x, Y = y}}))
                .Where(t => t.Char == position)
                .First()
                .Coordinate;
        }

        private static char[][] SecondKeypad = new char[][] {
            new char[] {' ', ' ', ' ', ' ', ' ', ' ', ' '},
            new char[] {' ', ' ', ' ', '1', ' ', ' ', ' '},
            new char[] {' ', ' ', '2', '3', '4', ' ', ' '},
            new char[] {' ', '5', '6', '7', '8', '9', ' '},
            new char[] {' ', ' ', 'A', 'B', 'C', ' ', ' '},
            new char[] {' ', ' ', ' ', 'D', ' ', ' ', ' '},
            new char[] {' ', ' ', ' ', ' ', ' ', ' ', ' '},
        };
    }
}