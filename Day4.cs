using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2016
{
    public class Day4
    {
        private static int numberOfLetters = 'z' - 'a' + 1;
        public static void Run()
        {
            var input = Misc.ReadInput(day: 4);
            var rooms = input.Select(row => new Room(row));
            var validRooms = rooms.Where(room => room.IsReal);
            var sectorIdSum = validRooms.Sum(room => room.SectorId);
            Console.WriteLine("Sector ID sum: {0}", sectorIdSum);

            var northPoleRoom = rooms.Where(room => DecryptRoomName(room).Contains("north")).FirstOrDefault();
            Console.WriteLine("North Pole room: {0}", DecryptRoomName(northPoleRoom));
        }

        private static string DecryptRoomName(Room room)
        {
            return string.Join("", room.Name.Select(letter => ShiftLetter(letter, room.SectorId)));
        }

        private static char ShiftLetter(char letter, int steps)
        {
            if (letter == '-')
                return ' ';
            return (char) ('a' + ((letter - 'a' + steps) % numberOfLetters));
        }

        public class Room
        {
            private static string roomPattern = @"^(.*)-(\d+)\[(\w+)\]$";
            private static Regex roomRegex = new Regex(roomPattern);

            public Room(string roomString)
            {
                var groups = roomRegex.Match(roomString).Groups;
                Name = groups[1].Value;
                SectorId = int.Parse(groups[2].Value);
                Checksum = groups[3].Value;
            }

            public string Name { get; set; }

            public int SectorId { get; set; }

            public string Checksum { get; set; }

            public bool IsReal
            {
                get
                {
                    return Name.Replace("-", "").Checksum(5) == Checksum;
                }
            }
        }
    }

    public static class Day4Extensions
    {
        public static string Checksum(this string str, int length)
        {
            var countedLetters = new Dictionary<char, int>();
            foreach (var letter in str)
            {
                if (!countedLetters.ContainsKey(letter))
                {
                    countedLetters[letter] = 0;
                }
                countedLetters[letter] += 1;
            }
            var pairs = countedLetters.ToList();
            pairs.Sort((kv1, kv2) => {
                return kv1.Value.CompareTo(kv2.Value) * 100 - kv1.Key.CompareTo(kv2.Key);
            });
            pairs.Reverse();
            return new string(pairs.Select(kv => kv.Key).Take(length).ToArray());
        }
    }
}