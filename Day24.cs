using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day24
    {
        public static void Run()
        {
            var rows = Misc.ReadInput(day: 24);

            // Part 1
            var board = new UpdatedBoard(rows);
            var shortestRouteLength = board.ShortestRouteLength();
            Console.WriteLine("Shortest route length: {0}", shortestRouteLength);

            // Part 2
            shortestRouteLength = board.ShortestRouteLengthWhenReturningToStart();
            Console.WriteLine("Shortest route length when returning to start: {0}", shortestRouteLength);
        }

        public class UpdatedBoard : Day13.Board
        {
            private static char _openSpace = '.';
            private static char _wall = '#';

            public UpdatedBoard(string[] rows) : base(rows[0].Count(), rows.Count())
            {
                InitCoordinatesAndFindPointsOfInterest(rows);
            }

            public IDictionary<int, (int X, int Y)> PointsOfInterest { get; } = new Dictionary<int, (int, int)>();

            private void InitCoordinatesAndFindPointsOfInterest(string[] rows)
            {
                for (var x = 0; x < Coordinates.GetLength(0); ++x)
                {
                    for (var y = 0; y < Coordinates.GetLength(1); ++y)
                    {
                        var value = rows[y][x];
                        Coordinates[x, y] = value != _wall;
                        if (value != _wall && value != _openSpace)
                            PointsOfInterest[int.Parse($"{value}")] = (x, y);
                    }
                }
            }

            public int ShortestRouteLength()
            {
                var distanceBetweenEachPair = DistanceBetweenEachPointOfInterestPair();
                return PointsOfInterestPermutations()
                    .Select(p => Enumerable
                        .Range(0, p.Count()-1)
                        .Select(i => distanceBetweenEachPair[(p[i], p[i+1])])
                        .Sum())
                    .Min();
            }

            public int ShortestRouteLengthWhenReturningToStart()
            {
                var distanceBetweenEachPair = DistanceBetweenEachPointOfInterestPair();
                return PointsOfInterestPermutations()
                    .Select(p => p.Concat(new [] { p[0] }).ToList())
                    .Select(p => Enumerable
                        .Range(0, p.Count()-1)
                        .Select(i => distanceBetweenEachPair[(p[i], p[i+1])])
                        .Sum())
                    .Min();
            }

            private Dictionary<(int, int), int> DistanceBetweenEachPointOfInterestPair()
            {
                var distanceBetweenEachPair = PointsOfInterest
                    .DifferentCombinations(2)
                    .Select(iter => iter.ToList())
                    .Select(pair => (pair[0].Key, pair[1].Key, ShortestPath(pair[0].Value, pair[1].Value).Count()-1))
                    .ToDictionary(t => (t.Item1, t.Item2), t => t.Item3);
                foreach (var kv in distanceBetweenEachPair.ToDictionary(kv => kv.Key, kv => kv.Value))
                {
                    distanceBetweenEachPair[(kv.Key.Item2, kv.Key.Item1)] = kv.Value;
                }
                return distanceBetweenEachPair;
            }

            private IEnumerable<List<int>> PointsOfInterestPermutations(int startAt = 0)
            {
                return PointsOfInterest.Keys
                    .Permutations(PointsOfInterest.Count())
                    .Select(iter => iter.ToList())
                    .Where(p => p[0] == startAt);
            }
        }
    }

    public static class Day24Extensions
    {
        /// <summary>
        /// Get all permutations of a collection.
        /// </summary>
        /// <remarks>
        /// Based on https://stackoverflow.com/a/10630026/595990.
        /// </remarks>
        public static IEnumerable<IEnumerable<T>> Permutations<T>(this IEnumerable<T> elements, int length)
        {
            if (length == 1)
                return elements.Select(t => new T[] { t });

            return Permutations(elements, length - 1)
                .SelectMany(t => elements.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }

        // https://stackoverflow.com/questions/33336540/how-to-use-linq-to-find-all-combinations-of-n-items-from-a-set-of-numbers
        public static IEnumerable<IEnumerable<T>> DifferentCombinations<T>(this IEnumerable<T> elements, int k)
        {
            return k == 0 ? new[] { new T[0] } :
            elements.SelectMany((e, i) =>
                elements.Skip(i + 1).DifferentCombinations(k - 1).Select(c => (new[] {e}).Concat(c)));
        }
    }
}