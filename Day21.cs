using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day21
    {
        public static void Run()
        {
            var scramblingInstructions = Misc.ReadInput(day: 21);

            // Part 1
            var password = "abcdefgh";
            var scrambledPassword = ScramblePassword(password, scramblingInstructions);
            Console.WriteLine("Scrambled password: {0}", scrambledPassword);

            // Part 2
            scrambledPassword = "fbgdceah";
            password = UnscramblePassword(scrambledPassword, scramblingInstructions);
            Console.WriteLine("Unscrambled password: {0}", password);
        }

        private static string ScramblePassword(string password, string[] scramblingInstructions)
        {
            var pw = password.ToList();
            foreach (var instruction in scramblingInstructions)
            {
                var splits = instruction.Split();
                switch (splits[0])
                {
                    case "swap":
                        {
                            switch (splits[1])
                            {
                                case "position":
                                    var firstPosition = splits[2].ToInt();
                                    var secondPosition = splits[5].ToInt();
                                    SwapPosition(pw, firstPosition, secondPosition);
                                    break;
                                case "letter":
                                    var firstLetter = splits[2][0];
                                    var secondLetter = splits[5][0];
                                    SwapLetter(pw, firstLetter, secondLetter);
                                    break;
                                default:
                                    throw new Exception("Unknown instruction");
                            }
                        }
                        break;
                    case "rotate":
                        {
                            switch (splits[1])
                            {
                                case "left":
                                    var steps = splits[2].ToInt();
                                    RotateLeft(pw, steps);
                                    break;
                                case "right":
                                    steps = splits[2].ToInt();
                                    RotateRight(pw, steps);
                                    break;
                                case "based":
                                    var letter = splits[6][0];
                                    var position = pw.IndexOf(letter);
                                    steps = position + 1 + (position >= 4 ? 1 : 0);
                                    RotateRight(pw, steps);
                                    break;
                            }
                        }
                        break;
                    case "reverse":
                        {
                            var firstPosition = splits[2].ToInt();
                            var secondPosition = splits[4].ToInt();
                            Reverse(pw, firstPosition, secondPosition);
                        }
                        break;
                    case "move":
                        {
                            var firstPosition = splits[2].ToInt();
                            var secondPosition = splits[5].ToInt();
                            Move(pw, firstPosition, secondPosition);
                        }
                        break;
                    default:
                        throw new Exception("Unknown instruction");
                }
            }
            return string.Join("", pw);
        }

        private static string UnscramblePassword(string password, string[] scramblingInstructions)
        {
            var pw = password.ToList();
            foreach (var instruction in scramblingInstructions.Reverse())
            {
                var splits = instruction.Split();
                switch (splits[0])
                {
                    case "swap":
                        {
                            switch (splits[1])
                            {
                                case "position":
                                    var firstPosition = splits[2].ToInt();
                                    var secondPosition = splits[5].ToInt();
                                    SwapPosition(pw, firstPosition, secondPosition);
                                    break;
                                case "letter":
                                    var firstLetter = splits[2][0];
                                    var secondLetter = splits[5][0];
                                    SwapLetter(pw, firstLetter, secondLetter);
                                    break;
                                default:
                                    throw new Exception("Unknown instruction");
                            }
                        }
                        break;
                    case "rotate":
                        {
                            switch (splits[1])
                            {
                                case "left":
                                    var steps = splits[2].ToInt();
                                    RotateRight(pw, steps);
                                    break;
                                case "right":
                                    steps = splits[2].ToInt();
                                    RotateLeft(pw, steps);
                                    break;
                                case "based":
                                    // TODO
                                    var endPw = pw.ToList();
                                    char letter;
                                    int position;
                                    while (true)
                                    {
                                        letter = splits[6][0];
                                        position = pw.IndexOf(letter);
                                        steps = position + 1 + (position >= 4 ? 1 : 0);
                                        var testPw = pw.ToList();
                                        RotateRight(testPw, steps);
                                        if (testPw.SequenceEqual(endPw))
                                            break;
                                        RotateLeft(pw, 1);
                                    }
                                    break;
                            }
                        }
                        break;
                    case "reverse":
                        {
                            var firstPosition = splits[2].ToInt();
                            var secondPosition = splits[4].ToInt();
                            Reverse(pw, firstPosition, secondPosition);
                        }
                        break;
                    case "move":
                        {
                            var firstPosition = splits[2].ToInt();
                            var secondPosition = splits[5].ToInt();
                            Move(pw, secondPosition, firstPosition);
                        }
                        break;
                    default:
                        throw new Exception("Unknown instruction");
                }
            }
            return string.Join("", pw);
        }

        private static void SwapPosition(IList<char> password, int firstPosition, int secondPosition)
        {
            var firstLetter = password[firstPosition];
            var secondLetter = password[secondPosition];
            password[firstPosition] = secondLetter;
            password[secondPosition] = firstLetter;
        }

        private static void SwapLetter(IList<char> password, char firstLetter, char secondLetter)
        {
            var firstPosition = password.IndexOf(firstLetter);
            var secondPosition = password.IndexOf(secondLetter);
            password[firstPosition] = secondLetter;
            password[secondPosition] = firstLetter;
        }

        private static void RotateLeft(IList<char> password, int steps)
        {
            for (var _ = steps; _ > 0; --_)
            {
                var firstLetter = password.First();
                password.RemoveAt(0);
                password.Add(firstLetter);
            }
        }

        private static void RotateRight(IList<char> password, int steps)
        {
            for (var _ = steps; _ > 0; --_)
            {
                var lastLetter = password.Last();
                password.RemoveAt(password.Count()-1);
                password.Insert(0, lastLetter);
            }
        }

        private static void Reverse(List<char> password, int firstPosition, int secondPosition)
        {
            var range = password.GetRange(firstPosition, secondPosition-firstPosition+1);
            range.Reverse();
            password.RemoveRange(firstPosition, secondPosition-firstPosition+1);
            password.InsertRange(firstPosition, range);
        }

        private static void Move(List<char> password, int firstPosition, int secondPosition)
        {
            var letter = password[firstPosition];
            password.RemoveAt(firstPosition);
            password.Insert(secondPosition, letter);
        }
    }

    public static class Day21Extensions
    {
        public static int ToInt(this string str)
        {
            return int.Parse(str);
        }
    }
}