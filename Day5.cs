using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode2016
{
    public class Day5
    {
        public static void Run()
        {
            var input = Misc.ReadInput(day: 5)[0];
            var password = FindPassword(input, 8);
            Console.WriteLine("Password: {0}", password);

            var secondPassword = FindPassword2(input);
            Console.WriteLine("Second password: {0}", secondPassword);
        }

        private static string FindPassword(string doorId, int passwordLength)
        {
            var algorithm = MD5.Create();
            var characters = Enumerable
                .Range(0, int.MaxValue)
                .Select(i => $"{doorId}{i}".Hash(algorithm))
                .Where(hash => hash.StartsWith("00000"))
                .Select(hash => hash[5])
                .Take(passwordLength);
            return string.Join("", characters);
        }

        private static string FindPassword2(string doorId)
        {
            var algorithm = MD5.Create();
            var password = new char[] { '_', '_', '_', '_', '_', '_', '_', '_' };
            var i = 0;
            while (true)
            {
                var hash = $"{doorId}{i}".Hash(algorithm);
                if (hash.StartsWith("00000") && hash[5] < '8')
                {
                    var j = hash[5] - '0';
                    if (password[j] == '_')
                        password[j] = hash[6];
                    if (!password.Contains('_'))
                        break;
                }
                ++i;
            }
            return string.Join("", password);
        }
    }

    public static class Day5Extensions
    {
        public static string Hash(this string str, HashAlgorithm algorithm)
        {
            return BitConverter.ToString(
                algorithm.ComputeHash(Encoding.ASCII.GetBytes(str))
            ).Replace("-", string.Empty);
        }
    }
}