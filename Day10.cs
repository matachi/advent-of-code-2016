using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day10
    {
        public static void Run()
        {
            var (bots, outputs, values) = CreateCollections(Misc.ReadInput(day: 10));
            int valuesThatHaveReachedTheirEnd = 0;
            while (valuesThatHaveReachedTheirEnd < values.Count())
            {
                foreach (var (id, value) in values)
                {
                    if (value.Move())
                    {
                        ++valuesThatHaveReachedTheirEnd;
                    }
                }
            }

            // Part 1
            var bot = bots.Values.FirstOrDefault(
                b => b.Values.Count() == 2 &&
                (b.Values[0].Id == 17 || b.Values[0].Id == 61) &&
                (b.Values[1].Id == 17 || b.Values[1].Id == 61));
            Console.WriteLine("Bot ID: {0}", bot?.Id);

            // Part 2
            var outputsToLookAt = new Output[] { outputs[0], outputs[1], outputs[2] };
            var product = outputsToLookAt
                .SelectMany(o => o.Values)
                .Select(v => v.Id)
                .Aggregate((i, j) => i * j);
            Console.WriteLine("Outputs values product: {0}", product);
        }

        private static (Dictionary<int, Bot>, Dictionary<int, Output>, Dictionary<int, Value>) CreateCollections(IEnumerable<string> input)
        {
            var bots = new Dictionary<int, Bot>();
            var outputs = new Dictionary<int, Output>();
            var values = new Dictionary<int, Value>();
            foreach (var row in input)
            {
                ParseRowAndAddEntitesAndValues(row, bots, outputs, values);
            }
            return (bots, outputs, values);
        }

        private static void ParseRowAndAddEntitesAndValues(string row, Dictionary<int, Bot> bots,
            Dictionary<int, Output> outputs, Dictionary<int, Value> values)
        {
            var splits = row.Split();
            var id = int.Parse(splits[1]);
            if (splits[0] == "bot")
            {
                var bot = AddBotIfNotAlreadyExists(id, bots);
                var lowId = int.Parse(splits[6]);
                var highId = int.Parse(splits[11]);
                if (splits[5] == "bot")
                {
                    bot.LowOutput = AddBotIfNotAlreadyExists(lowId, bots);
                }
                else if (splits[5] == "output")
                {
                    bot.LowOutput = AddOutputIfNotAlreadyExists(lowId, outputs);
                }
                if (splits[10] == "bot")
                {
                    bot.HighOutput = AddBotIfNotAlreadyExists(highId, bots);
                }
                else if (splits[10] == "output")
                {
                    bot.HighOutput = AddOutputIfNotAlreadyExists(highId, outputs);
                }
            }
            else if (splits[0] == "value")
            {
                var botId = int.Parse(splits[5]);
                var bot = AddBotIfNotAlreadyExists(botId, bots);
                var value = AddValueIfNotAlreadyExists(id, values);
                bot.Values.Add(value);
                value.HasBeenIn.Add(bot);
            }
            else
            {
                throw new ArgumentException("Invalid type");
            }
        }

        private static Bot AddBotIfNotAlreadyExists(int id, Dictionary<int, Bot> bots)
        {
            if (!bots.ContainsKey(id))
            {
                bots[id] = new Bot(id);
            }
            return bots[id];
        }

        private static Output AddOutputIfNotAlreadyExists(int id, Dictionary<int, Output> outputs)
        {
            if (!outputs.ContainsKey(id))
            {
                outputs[id] = new Output(id);
            }
            return outputs[id];
        }

        private static Value AddValueIfNotAlreadyExists(int id, Dictionary<int, Value> values)
        {
            if (!values.ContainsKey(id))
            {
                values[id] = new Value(id);
            }
            return values[id];
        }

        private abstract class Entity {
            public Entity(int id)
            {
                Id = id;
            }

            public int Id { get; }

            public IList<Value> Values { get; } = new List<Value>();
        }

        private class Bot : Entity
        {
            public Bot(int id) : base(id) {}

            public Entity LowOutput { get; set; }

            public Entity HighOutput { get; set; }

            public Entity Move(Value value)
            {
                if (Values.Count() < 2)
                {
                    return null;
                }
                var otherValue = Values[0].Id == value.Id ? Values[1] : Values[0];
                if (value.Id < otherValue.Id)
                {
                    LowOutput.Values.Add(value);
                    return LowOutput;
                }
                else
                {
                    HighOutput.Values.Add(value);
                    return HighOutput;
                }
            }
        }

        private class Output : Entity
        {
            public Output(int id) : base(id) {}
        }

        private class Value
        {
            public Value(int id)
            {
                Id = id;
            }

            public int Id { get; }

            public IList<Bot> HasBeenIn { get; } = new List<Bot>();

            public Output EndedIn { get; private set; }

            public bool Move()
            {
                if (EndedIn != null)
                {
                    return false;
                }
                var entity = HasBeenIn.Last().Move(this);
                if (entity == null)
                {
                    return false;
                }
                if (entity is Output output)
                {
                    EndedIn = output;
                    return true;
                }
                else if (entity is Bot bot)
                {
                    HasBeenIn.Add(bot);
                    return false;
                }
                throw new ApplicationException();
            }
        }
    }
}