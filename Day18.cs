using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day18
    {
        private static char trap = '^';
        private static char safe = '.';

        public static void Run()
        {
            var initialRow = Misc.ReadInput(day: 18).First();
            var numberOfSafeTiles = NumberOfSafeTiles(initialRow, 40);
            Console.WriteLine("Number of safe tiles with 40 rows: {0}", numberOfSafeTiles);

            numberOfSafeTiles = NumberOfSafeTiles(initialRow, 400000);
            Console.WriteLine("Number of safe tiles with 400000 rows: {0}", numberOfSafeTiles);
        }

        public static int NumberOfSafeTiles(string initialRow, int numberOfRows)
        {
            return GenerateRows(initialRow, numberOfRows).Select(row => row.Where(chr => chr == safe).Count()).Sum();
        }

        private static IEnumerable<string> GenerateRows(string initialRow, int numberOfRows)
        {
            yield return initialRow;
            var previousRow = $"{safe}{initialRow}{safe}";
            for (var _ = 1; _ < numberOfRows; ++_)
            {
                var generatedChars = Enumerable
                    .Range(1, previousRow.Count()-2)
                    .Select(i => IsTrap(previousRow[i-1], previousRow[i], previousRow[i+1]) ? trap : safe);
                var row = string.Join("", generatedChars);
                yield return row;
                previousRow = $"{safe}{row}{safe}";
            }
        }

        private static bool IsTrap(char left, char center, char right)
        {
            if (left == trap && center == trap && right == safe)
                return true;
            if (left == safe && center == trap && right == trap)
                return true;
            if (left == trap && center == safe && right == safe)
                return true;
            if (left == safe && center == safe && right == trap)
                return true;
            return false;
        }
    }
}