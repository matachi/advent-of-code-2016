using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day7
    {
        public static void Run()
        {
            var input = Misc.ReadInput(day: 7).ToList();
            var ipAddressesThatSupportTls = input.Where(ip => SupportsTls(ip));
            Console.WriteLine("Number of IP addresses with TLS: {0}", ipAddressesThatSupportTls.Count());
            var ipAddressesThatSupportSsl = input.Where(ip => SupportsSsl(ip));
            Console.WriteLine("Number of IP addresses with SSL: {0}", ipAddressesThatSupportSsl.Count());
        }

        private static bool SupportsTls(string ip)
        {
            var i = 0;
            while (true)
            {
                i = ip.IndexOf('[', i + 1);
                if (i == -1)
                    break;
                var j = ip.IndexOf(']', i);
                if (ip.Substring(i, j - i).ContainsEvenPalindrome())
                    return false;
            }

            i = 0;
            while (true)
            {
                var quitNext = false;
                var j = ip.IndexOf('[', i);
                if (j == -1)
                {
                    j = ip.Count();
                    quitNext = true;
                }
                if (ip.Substring(i, j - i).ContainsEvenPalindrome())
                    return true;
                if (quitNext)
                    break;
                i = ip.IndexOf(']', j) + 1;
            }

            return false;
        }

        private static bool SupportsSsl(string ip)
        {
            var splits = StringsOutsideAndInsideBrackets(ip);
            var stringsOutsideBrackets = splits.Item1;
            var stringsInsideBrackets = splits.Item2;
            var threeLetterSequencesOutside = stringsOutsideBrackets.SelectMany(str => ThreeLetterSequences(str));
            var threeLetterSequencesInside = stringsInsideBrackets.SelectMany(str => ThreeLetterSequences(str));
            var palindromesOutside = threeLetterSequencesOutside.Where(seq => seq.ContainsOddPalindrome());
            var palindromesInside = threeLetterSequencesInside.Where(seq => seq.ContainsOddPalindrome());
            foreach (var palindromeOutside in palindromesOutside)
            {
                foreach (var palindromeInside in palindromesInside)
                {
                    if (palindromeOutside[0] == palindromeInside[1] &&
                        palindromeOutside[1] == palindromeInside[0])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static Tuple<IList<string>, IList<string>> StringsOutsideAndInsideBrackets(string str)
        {
            var splits = str.Split(new []{'[', ']'});
            return new Tuple<IList<string>, IList<string>>(
                splits.Where((s, i) => i % 2 == 0).ToList(),
                splits.Where((s, i) => i % 2 == 1).ToList()
            );
        }

        private static IEnumerable<string> ThreeLetterSequences(string str)
        {
            return Enumerable.Range(0, str.Length - 2).Select(i => str.Substring(i, 3));
        }
    }

    public static class Day7Extensions
    {
        public static bool ContainsEvenPalindrome(this string str)
        {
            for (var i = 0; i < str.Count() - 3; ++i)
            {
                if (str[i] == str[i + 3] && str[i] != str[i + 1] && str[i + 1] == str[i + 2])
                    return true;
            }
            return false;
        }

        public static bool ContainsOddPalindrome(this string str)
        {
            for (var i = 0; i < str.Count() - 2; ++i)
            {
                if (str[i] == str[i + 2])
                    return true;
            }
            return false;
        }
    }
}