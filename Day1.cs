using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day1
    {
        public static void Run()
        {
            var content = Misc.ReadInput(day: 1)[0]
                .Split(new string[] {", "}, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => new {
                    Rotation = s[0] == 'L' ? Rotation.Left : Rotation.Right,
                    Steps = int.Parse(s.Substring(1))
                });

            Coordinate? firstLocationVisitedTwice = null;
            var visitedCoordinates = new List<Coordinate>{ new Coordinate { X = 0, Y = 0 } };
            var currentDirection = Direction.North;
            foreach (var tuple in content)
            {
                currentDirection = (Direction) (
                    (((int) currentDirection) + 4 + (tuple.Rotation == Rotation.Right ? 1 : -1)) % 4);
                var newCoordinates = new List<Coordinate>();
                Func<int, Coordinate> newCoordinateFunc;
                switch (currentDirection)
                {
                    case Direction.North:
                        newCoordinateFunc = (step) => new Coordinate { X = 0, Y = step };
                        break;
                    case Direction.South:
                        newCoordinateFunc = (step) => new Coordinate { X = 0, Y = -step };
                        break;
                    case Direction.East:
                        newCoordinateFunc = (step) => new Coordinate { X = step, Y = 0 };
                        break;
                    case Direction.West:
                        newCoordinateFunc = (step) => new Coordinate { X = -step, Y = 0 };
                        break;
                    default:
                        throw new ArgumentException("Invalid direction");
                }
                newCoordinates
                    .AddRange(Enumerable.Range(1, tuple.Steps)
                    .Select(step => visitedCoordinates.Last() + newCoordinateFunc.Invoke(step)));
                if (!firstLocationVisitedTwice.HasValue)
                {
                    foreach (var newCoordinate in newCoordinates)
                    {
                        if (visitedCoordinates.Contains(newCoordinate))
                        {
                            firstLocationVisitedTwice = newCoordinate;
                            break;
                        }
                    }
                }
                visitedCoordinates.AddRange(newCoordinates);
            }

            Console.WriteLine("Distance: {0}", visitedCoordinates.Last().Distance);
            Console.WriteLine("Distance to first location visited twice: {0}", firstLocationVisitedTwice?.Distance);
        }
    }

    public enum Rotation
    {
        Left, Right
    }

    public enum Direction
    {
        North, East, South, West
    }

    public struct Coordinate
    {
        public int X;
        public int Y;

        public int Distance => Math.Abs(X) + Math.Abs(Y);

        public static Coordinate operator +(Coordinate c1, Coordinate c2)
        {
            return new Coordinate { X = c1.X + c2.X, Y = c1.Y + c2.Y };
        }

        public override string ToString()
        {
            return $" X = {X}, Y = {Y} ";
        }
    }
}
