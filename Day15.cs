using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day15
    {
        public static void Run()
        {
            // Part 1
            var firstTimeToPressButton = FirstTimeYouCanPressButton(InitDiscs());
            Console.WriteLine("First time you can press the button: {0}", firstTimeToPressButton);

            // Part 2
            var discs = InitDiscs();
            discs.Add(new Disc{ Id = 7, Positions = 11, Start = 0 });
            var firstTimeToPressButton2 = FirstTimeYouCanPressButton(discs);
            Console.WriteLine("First time you can press the button: {0}", firstTimeToPressButton2);
        }

        public static int FirstTimeYouCanPressButton(IList<Disc> discs)
        {
            foreach (var disc in discs)
            {
                disc.CompensateForWhenCapsuleReachesDisc();
            }
            for (var time = 0;; ++time)
            {
                if (discs.All(disc => disc.IsAlignedAtTime(time)))
                    return time;
            }
        }

        private static IList<Disc> InitDiscs()
        {
            var discs = new List<Disc>();
            discs.Add(new Disc{ Id = 1, Positions = 7, Start = 0 });
            discs.Add(new Disc{ Id = 2, Positions = 13, Start = 0 });
            discs.Add(new Disc{ Id = 3, Positions = 3, Start = 2 });
            discs.Add(new Disc{ Id = 4, Positions = 5, Start = 2 });
            discs.Add(new Disc{ Id = 5, Positions = 17, Start = 0 });
            discs.Add(new Disc{ Id = 6, Positions = 19, Start = 7 });
            return discs;
        }

        public class Disc
        {
            public int Id { get; set; }
            public int Positions { get; set; }
            public int Start { get; set; }

            public void CompensateForWhenCapsuleReachesDisc()
            {
                Start = (Start + Id) % Positions;
            }

            public bool IsAlignedAtTime(int time)
            {
                return (Start + time) % Positions == 0;
            }
        }
    }
}