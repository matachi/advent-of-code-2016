using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day16
    {
        public static void Run()
        {
            var puzzleInput = "01000100010010111";

            // Part 1
            var checksum = Checksum(DataFillingDisk(puzzleInput, 272));
            Console.WriteLine("Checksum: {0}", checksum);

            // Part 2
            checksum = Checksum(DataFillingDisk(puzzleInput, 35651584));
            Console.WriteLine("Checksum: {0}", checksum);
        }

        private static string DataFillingDisk(string input, int length)
        {
            while (input.Count() < length)
            {
                input = ModifiedDragonCurve(input);
            }
            return input.Substring(0, length);
        }

        private static string ModifiedDragonCurve(string a)
        {
            var b = string.Join("", a.Reverse().Select(c => c == '0' ? '1' : '0'));
            return $"{a}0{b}";
        }

        private static string Checksum(string data)
        {
            var checksum = string.Join("", Enumerable.Range(0, data.Count() / 2)
                .Select(i => data[2*i] == data[2*i+1] ? '1' : '0'));
            if (checksum.Count() % 2 == 0)
                return Checksum(checksum);
            return checksum;
        }
    }
}