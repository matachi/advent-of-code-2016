using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace AdventOfCode2016
{
    public class Day17
    {
        private static HashAlgorithm _algorithm = MD5.Create();
        private static int _shortestPathLength = int.MaxValue;
        private static IList<(int X, int Y)> _shortestPath;

        public static void Run()
        {
            var puzzleInput = "pvhmgsws";

            var shortestPath = FindShortestPathToExit(puzzleInput);
            Console.WriteLine("Shortest path: {0}", shortestPath);

            var longestPathLength = FindLongestPathLengthToExit(puzzleInput);
            Console.WriteLine("Longest path length: {0}", longestPathLength);
        }

        public static string FindShortestPathToExit(string passcode)
        {
            _shortestPathLength = int.MaxValue;
            _shortestPath = null;

            var startCoordinate = (0, 0);
            var path = new Stack<(int X, int Y)>(new []{ startCoordinate });
            FindShortestPathToExit(passcode, path);
            var directions = Enumerable
                .Range(1, _shortestPath.Count()-1)
                .Select(i => GetDirection(_shortestPath[i-1], _shortestPath[i]));
            return string.Join("", directions);
        }

        private static void FindShortestPathToExit(string passcode, Stack<(int X, int Y)> path)
        {
            if (path.Count() >= _shortestPathLength)
                return;
            if (IsAtExit(path.Peek()))
            {
                _shortestPath = path.Reverse().ToList();
                _shortestPathLength = _shortestPath.Count();
                return;
            }
            foreach (var (direction, coordinate) in Directions(path.Peek(), passcode))
            {
                path.Push(coordinate);
                var newPasscode = passcode + direction;
                FindShortestPathToExit(newPasscode, path);
                path.Pop();
            }
        }

        public static int FindLongestPathLengthToExit(string passcode)
        {
            var startCoordinate = (0, 0);
            var path = new Stack<(int X, int Y)>(new []{ startCoordinate });
            return FindLongestPathLengthToExit(passcode, path);
        }

        private static int FindLongestPathLengthToExit(string passcode, Stack<(int X, int Y)> path)
        {
            if (IsAtExit(path.Peek()))
            {
                _shortestPath = path.Reverse().ToList();
                _shortestPathLength = _shortestPath.Count();
                return 0;
            }
            var longestChild = int.MinValue;
            foreach (var (direction, coordinate) in Directions(path.Peek(), passcode))
            {
                path.Push(coordinate);
                var newPasscode = passcode + direction;
                longestChild = Math.Max(longestChild, FindLongestPathLengthToExit(newPasscode, path) + 1);
                path.Pop();
            }
            return longestChild;
        }

        private static bool IsAtExit((int X, int Y) coordinate)
        {
            return coordinate.X == 3 && coordinate.Y == 3;
        }

        private static IEnumerable<(char Direction, (int X, int Y) Coordinate)> Directions((int X, int Y) coordinate, string passcode)
        {
            var hash = passcode.Hash(_algorithm);
            if (coordinate.Y > 0 && hash[0] >= 'B' && hash[0] <= 'F')
                yield return ('U', (coordinate.X, coordinate.Y - 1));
            if (coordinate.Y < 3 && hash[1] >= 'B' && hash[1] <= 'F')
                yield return ('D', (coordinate.X, coordinate.Y + 1));
            if (coordinate.X > 0 && hash[2] >= 'B' && hash[2] <= 'F')
                yield return ('L', (coordinate.X - 1, coordinate.Y));
            if (coordinate.X < 3 && hash[3] >= 'B' && hash[3] <= 'F')
                yield return ('R', (coordinate.X + 1, coordinate.Y));
        }

        private static char GetDirection((int X, int Y) coordinate, (int X, int Y) nextCoordinate)
        {
            if (nextCoordinate.X - coordinate.X > 0)
                return 'R';
            if (nextCoordinate.X - coordinate.X < 0)
                return 'L';
            if (nextCoordinate.Y - coordinate.Y > 0)
                return 'D';
            if (nextCoordinate.Y - coordinate.Y < 0)
                return 'U';
            throw new Exception("The coordinates are the same.");
        }
    }
}