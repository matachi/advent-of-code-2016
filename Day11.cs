using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day11
    {
        public static void Run()
        {
            // Part 1
            var configuration = Configuration.InitialConfiguration();
            var minimumNumberOfSteps = FindMinimumNumberOfSteps(configuration);
            Console.WriteLine("Minimum number of steps: {0}", minimumNumberOfSteps);

            // Part 2
            configuration = Configuration.InitialConfiguration();
            configuration[0].Add(-(int) Rock.Elerium);
            configuration[0].Add((int) Rock.Elerium);
            configuration[0].Add(-(int) Rock.Dilithium);
            configuration[0].Add((int) Rock.Dilithium);
            minimumNumberOfSteps = FindMinimumNumberOfSteps(configuration);
            Console.WriteLine("Minimum number of steps: {0}", minimumNumberOfSteps);
        }

        public static int FindMinimumNumberOfSteps(Configuration configuration)
        {
            var seen = new HashSet<string>();
            seen.Add(configuration.Hash(configuration.Elevator));

            var discovered = new Queue<Configuration>();
            discovered.Enqueue(configuration);

            while (true)
            {
                var current = discovered.Dequeue();

                if (current.IsDone())
                    return current.Step;

                for (var i = 0; i < current[current.Elevator].Count(); ++i)
                {
                    for (var j = i+1; j < current[current.Elevator].Count(); ++j)
                    {
                        ExploreConfiguration(current, 1, seen, discovered, i, j);
                        ExploreConfiguration(current, -1, seen, discovered, i, j);
                    }
                    ExploreConfiguration(current, 1, seen, discovered, i);
                    ExploreConfiguration(current, -1, seen, discovered, i);
                }
            }
        }

        private static void ExploreConfiguration(Configuration configuration, int direction, HashSet<string> seen,
            Queue<Configuration> discovered, int firstItem, int? secondItem = null)
        {
            var newElevatorIndex = configuration.Elevator + direction;
            if (newElevatorIndex < 0 || newElevatorIndex >= configuration.Count())
                return;

            if (secondItem.HasValue)
                configuration.MoveItems(configuration.Elevator, direction, secondItem.Value, 0);
            configuration.MoveItems(configuration.Elevator, direction, firstItem, 0);

            var hash = configuration.Hash(newElevatorIndex);
            if (!seen.Contains(hash))
            {
                seen.Add(hash);
                if (configuration.IsValid(configuration.Elevator) && configuration.IsValid(newElevatorIndex))
                {
                    var newConfiguration = (Configuration) configuration.Clone();
                    newConfiguration.Elevator = newElevatorIndex;
                    newConfiguration.Step += 1;
                    discovered.Enqueue(newConfiguration);
                }
            }

            configuration.MoveItems(newElevatorIndex, -direction, 0, firstItem);
            if (secondItem.HasValue)
                configuration.MoveItems(newElevatorIndex, -direction, 0, secondItem.Value);
        }

        public class Configuration : List<List<int>>, ICloneable
        {
            public static Configuration InitialConfiguration()
            {
                var configuration = new Configuration();

                configuration.Add(new List<int>());
                configuration.Add(new List<int>());
                configuration.Add(new List<int>());
                configuration.Add(new List<int>());
                configuration[0].Add(-(int) Rock.Cobalt);
                configuration[0].Add(-(int) Rock.Polonium);
                configuration[0].Add(-(int) Rock.Promethium);
                configuration[0].Add(-(int) Rock.Ruthenium);
                configuration[0].Add(-(int) Rock.Thulium);
                configuration[0].Add((int) Rock.Cobalt);
                configuration[0].Add((int) Rock.Ruthenium);
                configuration[0].Add((int) Rock.Thulium);
                configuration[1].Add((int) Rock.Polonium);
                configuration[1].Add((int) Rock.Promethium);

                return configuration;
            }

            public int Elevator { get; set; }

            public int Step { get; set; }

            public bool IsValid(int floorIndex)
            {
                var floor = this[floorIndex];
                var hasGenerators = floor.Any(i => i < 0);
                var hasUnconnectedMicroship = floor.Where(i => i > 0).Where(i => !floor.Contains(i)).Any();
                return !(hasGenerators && hasUnconnectedMicroship);
            }

            public bool IsDone()
            {
                return !this[0].Any() && !this[1].Any() && !this[2].Any();
            }

            public void MoveItems(int fromFloor, int direction, int currentItemIndex, int newItemIndex)
            {
                this[fromFloor+direction].Insert(newItemIndex, this[fromFloor][currentItemIndex]);
                this[fromFloor].RemoveAt(currentItemIndex);
            }

            public string Hash(int elevator)
            {
                var floors = this.Select(floor => $"{floor.Count()},{floor.Where(i => i < 0).Count()}");
                return $"{elevator}-{string.Join("", floors)}";
            }

            public object Clone()
            {
                var configuration = new Configuration();
                configuration.Add(this[0].ToList());
                configuration.Add(this[1].ToList());
                configuration.Add(this[2].ToList());
                configuration.Add(this[3].ToList());
                configuration.Elevator = Elevator;
                configuration.Step = Step;
                return configuration;
            }
        }
    }

    public enum Rock
    {
        Cobalt = 1,
        Polonium,
        Promethium,
        Ruthenium,
        Thulium,
        Elerium,
        Dilithium
    }
}