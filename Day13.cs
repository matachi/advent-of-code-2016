using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day13
    {
        public static void Run()
        {
            // The puzzle input
            var input = 1362;

            // Part 1
            var board = new Board(favoriteNumber: input);
            var shortestPath = board.ShortestPath(start: (1, 1), goal: (31, 39));
            // Don't count the starting node
            var numberOfSteps = shortestPath.Count() - 1;
            Console.WriteLine("Shortest path length: {0}", numberOfSteps);

            // Part 2
            var coordinateReachability = board.ReachableWithinNumberOfSteps(start: (1, 1));
            var numberOfCoordinatesReachableWithinFiftySteps = (
                from cell in coordinateReachability.Cells()
                where cell.Value <= 50
                select cell).Count();
            Console.WriteLine("Number of coordinates that can be reached within 50 steps: {0}",
                numberOfCoordinatesReachableWithinFiftySteps);
        }

        public class Board
        {
            /// <summary>
            /// Create the game board.
            /// </summary>
            /// <param name="favoriteNumber">The location of open spaces and walls depend on this puzzle input.</param>
            public Board(int favoriteNumber) : this(100, 100)
            {
                InitCoordinates(favoriteNumber);
            }

            public Board(int width, int height)
            {
                Coordinates = new bool[width, height];
            }

            /// <summary>
            /// true = open space, false = wall
            /// </summary>
            protected bool[,] Coordinates { get; }

            /// <summary>
            /// Initialize the board's coordinates with open spaces and falls according to the game's rules.
            /// </summary>
            private void InitCoordinates(int favoriteNumber)
            {
                for (var x = 0; x < Coordinates.GetLength(0); ++x)
                {
                    for (var y = 0; y < Coordinates.GetLength(1); ++y)
                    {
                        var value = x*x + 3*x + 2*x*y + y + y*y;
                        value += favoriteNumber;
                        var binaryRepresentation = Convert.ToString(value, 2);
                        if (binaryRepresentation.Where(c => c == '1').Count() % 2 == 0)
                            Coordinates[x, y] = true;
                    }
                }
            }

            /// <summary>
            /// Find the shortest path using the A* algorithm.
            /// </summary>
            /// <remarks>
            /// C# implementation of the A* algorithm found on
            /// https://en.wikipedia.org/w/index.php?title=A*_search_algorithm&oldid=790104875#Pseudocode
            /// </remarks>
            public IEnumerable<(int, int)> ShortestPath((int X, int Y) start, (int X, int Y) goal)
            {
                var closedSet = new HashSet<(int X, int Y)>();

                var openSet = new HashSet<(int X, int Y)>(new []{start});

                var cameFrom = new Dictionary<(int X, int Y), (int X, int Y)>();

                var gScore = new int[Coordinates.GetLength(0), Coordinates.GetLength(1)].InitializeWithValue(int.MaxValue);
                gScore[start.X, start.Y] = 0;

                var fScore = new int[Coordinates.GetLength(0), Coordinates.GetLength(1)].InitializeWithValue(int.MaxValue);
                fScore[start.X, start.Y] = HeuristicCostEstimate(start, goal);

                while (openSet.Any())
                {
                    var current = openSet.Lowest(coord => fScore[coord.X, coord.Y]);
                    if (current.X == goal.X && current.Y == goal.Y)
                        return ReconstructPath(cameFrom, current);

                    openSet.Remove(current);
                    closedSet.Add(current);

                    foreach (var neighbor in Neighbors(current))
                    {
                        if (closedSet.Any(c => c.X == neighbor.X && c.Y == neighbor.Y))
                            continue;

                        if (!openSet.Any(c => c.X == neighbor.X && c.Y == neighbor.Y))
                            openSet.Add(neighbor);

                        var tentativeGscore = gScore[current.X, current.Y] + DistanceBetween(current, neighbor);
                        if (tentativeGscore >= gScore[neighbor.X, neighbor.Y])
                            continue;

                        cameFrom[neighbor] = current;
                        gScore[neighbor.X, neighbor.Y] = tentativeGscore;
                        fScore[neighbor.X, neighbor.Y] = gScore[neighbor.X, neighbor.Y] + HeuristicCostEstimate(neighbor, goal);
                    }
                }
                throw new SystemException("No path found.");
            }

            /// <summary>
            /// Get the neighbors of a coordinate inside the allowable travel space.
            /// </summary>
            private IEnumerable<(int X, int Y)> Neighbors((int X, int Y) coordinate)
            {
                if (coordinate.X > 0)
                    yield return (coordinate.X - 1, coordinate.Y);
                if (coordinate.X < Coordinates.GetLength(0) - 1)
                    yield return (coordinate.X + 1, coordinate.Y);
                if (coordinate.Y > 0)
                    yield return (coordinate.X, coordinate.Y - 1);
                if (coordinate.Y < Coordinates.GetLength(1) - 1)
                    yield return (coordinate.X, coordinate.Y + 1);
            }

            /// <summary>
            /// Calculate the distance between to adjacent coordinates.
            /// </summary>
            private int DistanceBetween((int X, int Y) current, (int X, int Y) neighbor)
            {
                // Since we will always check the distance between two adjacent coordinates, it's enough to see if
                // the neighbor is a wall or not. If it's a wall we give it a large penalty, otherwise the distance
                // is one unit.
                return (Coordinates[neighbor.X, neighbor.Y]) ? 1 : 10000;
            }

            /// <summary>
            /// Estimate the distance between two coordinates by calculating the linear distance between them.
            /// </summary>
            private static int HeuristicCostEstimate((int X, int Y) start, (int X, int Y) goal)
            {
                return Math.Abs(goal.Y - start.Y) + Math.Abs(goal.X - start.X);
            }

            /// <summary>
            /// Construct the shortest path from the starting coordinate to the coordinate `current`.
            /// </summary>
            private static IEnumerable<(int X, int Y)> ReconstructPath(IDictionary<(int X, int Y), (int X, int Y)> cameFrom, (int X, int Y) current)
            {
                // Start at current.
                var path = new List<(int, int)>(new []{ current });
                // Walk backwards until we reach the starting coordinate.
                while (cameFrom.ContainsKey(current))
                {
                    current = cameFrom[current];
                    path.Insert(0, current);
                }
                return path;
            }

            /// <summary>
            /// Get the coordinate system where each coordinate's value is the number of steps it can be reached within.
            /// </summary>
            /// <remarks>
            /// This method is a modified version of the A* algorithm. It could share a lot of code with the method
            /// `ShortestPath` to reduce code duplication, but to keep them both simple and clean I prefer them being
            /// separate.
            /// </remarks>
            public int[,] ReachableWithinNumberOfSteps((int X, int Y) start)
            {
                var closedSet = new HashSet<(int X, int Y)>();

                var openSet = new HashSet<(int X, int Y)>(new []{start});

                var cameFrom = new Dictionary<(int X, int Y), (int X, int Y)>();

                var gScore = new int[Coordinates.GetLength(0), Coordinates.GetLength(1)].InitializeWithValue(int.MaxValue);
                gScore[start.X, start.Y] = 0;

                while (openSet.Any())
                {
                    var current = openSet.Lowest(coord => gScore[coord.X, coord.Y]);

                    openSet.Remove(current);
                    closedSet.Add(current);

                    foreach (var neighbor in Neighbors(current))
                    {
                        if (closedSet.Any(c => c.X == neighbor.X && c.Y == neighbor.Y))
                            continue;

                        if (!openSet.Any(c => c.X == neighbor.X && c.Y == neighbor.Y))
                            openSet.Add(neighbor);

                        var tentativeGscore = gScore[current.X, current.Y] + DistanceBetween(current, neighbor);
                        if (tentativeGscore >= gScore[neighbor.X, neighbor.Y])
                            continue;

                        cameFrom[neighbor] = current;
                        gScore[neighbor.X, neighbor.Y] = tentativeGscore;
                    }
                }

                return gScore;
            }
        }
    }

    public static class Day13Extensions
    {
        /// <summary>
        /// Initialize all coordinates in a 2D-array with a given value.
        /// </summary>
        public static T[,] InitializeWithValue<T>(this T[,] array, T value)
        {
            for (var x = 0; x < array.GetLength(0); ++x)
            {
                for (var y = 0; y < array.GetLength(1); ++y)
                {
                    array[x, y] = value;
                }
            }
            // Returns the same instance of the array to support method chaining.
            return array;
        }

        /// <summary>
        /// Return the element with the lowest score according to the `validate` function.
        /// </summary>
        public static T Lowest<T>(this IEnumerable<T> source, Func<T, int> validate)
        {
            IList<T> collection;
            if (source is IList<T> s)
                collection = s;
            else
                collection = source.ToList();
            var values = collection.Select(e => validate(e)).ToList();
            var lowestScore = values.Min();
            for (var i = 0; i < collection.Count(); ++i)
            {
                if (values[i] == lowestScore)
                    return collection[i];
            }
            throw new SystemException();
        }

        /// <summary>
        /// Iterate over the cells in a 2D-array.
        /// </summary>
        public static IEnumerable<(int X, int Y, T Value)> Cells<T>(this T[,] array)
        {
            for (var x = 0; x < array.GetLength(0); ++x)
            {
                for (var y = 0; y < array.GetLength(1); ++y)
                {
                    yield return (x, y, array[x, y]);
                }
            }
        }
    }
}