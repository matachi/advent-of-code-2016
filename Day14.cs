using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace AdventOfCode2016
{
    public class Day14
    {
        private static HashAlgorithm _algorithm = MD5.Create();
        private static string _sequencePattern = @".*?(.)\1\1.*";
        private static Regex _sequenceRegex = new Regex(_sequencePattern);

        public static void Run()
        {
            var puzzleInput = "ahsbgdzn";

            // Part 1
            var indexOfKey = FindIndexOfKey(puzzleInput);
            Console.WriteLine("Key index: {0}", indexOfKey);

            // Part 2
            var indexOfKeyWithKeyStreching = FindIndexOfKey(puzzleInput, 2016);
            Console.WriteLine("Key index with key stretching: {0}", indexOfKeyWithKeyStreching);
        }

        public static int FindIndexOfKey(string salt, int keyStreching = 0)
        {
            var potentialKeys = new List<(int Index, string NextSequence)>();
            var foundKeys = 0;
            for (var index = 0;; ++index)
            {
                var hash = (salt + index).Hash(_algorithm).ToLower();
                for (var _ = 0; _ < keyStreching; ++_)
                    hash = hash.ToLower().Hash(_algorithm).ToLower();
                var groups = _sequenceRegex.Match(hash).Groups;
                var sequence = groups[1].Value;
                if (sequence == "")
                    continue;

                var repeatedChar = sequence[0];
                potentialKeys.Add((index, string.Format("{0}{0}{0}{0}{0}", repeatedChar)));

                foreach (var potentialKey in potentialKeys.ToList())
                {
                    if (index > potentialKey.Index + 1000)
                    {
                        potentialKeys.Remove(potentialKey);
                        continue;
                    }
                    if (potentialKey.Index == index)
                        continue;
                    if (hash.Contains(potentialKey.NextSequence))
                    {
                        ++foundKeys;
                        if (foundKeys == 64)
                            return potentialKey.Index;
                        potentialKeys.Remove(potentialKey);
                    }
                }
            }
        }
    }
}