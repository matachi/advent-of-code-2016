using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day12
    {
        public static void Run()
        {
            var sourceCode = Misc.ReadInput(day: 12);

            // Part 1
            var assemblyProgram = new AssemblyProgram(sourceCode);
            assemblyProgram.Execute();
            Console.WriteLine("Value in register A: {0}", assemblyProgram.A.Value);

            // Part 2
            assemblyProgram = new AssemblyProgram(sourceCode);
            assemblyProgram.C.Value = 1;
            assemblyProgram.Execute();
            Console.WriteLine("Value in register A when C starts with value 1: {0}", assemblyProgram.A.Value);
        }
    }

    public class AssemblyProgram
    {
        public AssemblyProgram(IEnumerable<string> instructions)
        {
            foreach (var instruction in instructions)
            {
                Instructions.Add(ParseInstruction(instruction));
            }
        }

        public Register A { get; } = new Register();

        public Register B { get; } = new Register();

        public Register C { get; } = new Register();

        public Register D { get; } = new Register();

        public int InstructionPointer { get; set; } = 0;

        public IList<Instruction> Instructions { get; } = new List<Instruction>();

        public void Execute()
        {
            while (InstructionPointer >= 0 && InstructionPointer < Instructions.Count())
            {
                InstructionPointer += Instructions[InstructionPointer].Execute();
            }
        }

        protected virtual Instruction ParseInstruction(string code)
        {
            var splits = code.Split();
            switch (splits[0])
            {
                case "cpy":
                    return new Copy(GetValueSource(splits[1]), GetValueSource(splits[2]));
                case "inc":
                    return new Increment(GetValueSource(splits[1]));
                case "dec":
                    return new Decrement(GetValueSource(splits[1]));
                case "jnz":
                    return new JumpIfNotZero(GetValueSource(splits[1]), GetValueSource(splits[2]));
            }
            throw new ArgumentException("Unknown instruction.");
        }

        protected ValueSource GetValueSource(string name)
        {
            switch (name)
            {
                case "a":
                    return A;
                case "b":
                    return B;
                case "c":
                    return C;
                case "d":
                    return D;
                default:
                    return new Constant(int.Parse(name));
            }
        }

        public interface ValueSource
        {
            int Value { get; }
        }

        public class Register : ValueSource
        {
            public int Value { get; set; }
        }

        public class Constant : ValueSource
        {
            public Constant(int value)
            {
                Value = value;
            }

            public int Value { get; }
        }

        public interface Instruction
        {
            int Execute();
        }

        public class Copy : Instruction
        {
            public Copy(ValueSource from, ValueSource to)
            {
                From = from;
                To = to;
            }

            public ValueSource From { get; }

            public ValueSource To { get; }

            public int Execute()
            {
                if (To is Register r)
                    r.Value = From.Value;
                return 1;
            }
        }

        public class Increment : Instruction
        {
            public Increment(ValueSource valueSource)
            {
                ValueSource = valueSource;
            }

            public ValueSource ValueSource { get; }

            public int Execute()
            {
                if (ValueSource is Register r)
                    ++r.Value;
                return 1;
            }
        }

        public class Decrement : Instruction
        {
            public Decrement(ValueSource valueSource)
            {
                ValueSource = valueSource;
            }

            public ValueSource ValueSource { get; }

            public int Execute()
            {
                if (ValueSource is Register r)
                    --r.Value;
                return 1;
            }
        }

        public class JumpIfNotZero : Instruction
        {
            public JumpIfNotZero(ValueSource valueSource, ValueSource steps)
            {
                ValueSource = valueSource;
                Steps = steps;
            }

            public ValueSource ValueSource { get; }

            public ValueSource Steps { get; }

            public int Execute()
            {
                if (ValueSource.Value != 0)
                    return Steps.Value;
                return 1;
            }
        }
    }
}