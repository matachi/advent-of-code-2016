using System;
using System.IO;

namespace AdventOfCode2016
{
    public class Misc
    {
        public static string[] ReadInput(int day) {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "input", $"day{day}", "input");
            var content = File.ReadAllLines(path);
            return content;
        }

        public static string[] ReadInputFromUnittest(int day) {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "..", "..", "..", "input", $"day{day}", "input");
            var content = File.ReadAllLines(path);
            return content;
        }
    }
}