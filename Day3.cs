using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day3
    {
        public static void Run()
        {
            var input = Misc.ReadInput(day: 3);
            var rows = input
                .Select(row => row
                    .Split(' ')
                    .Where(str => str != "")
                    .Select(str => int.Parse(str))
                    .ToList())
                .ToList();
            var validTriangles = rows.Where(row => row.IsTriangle());
            Console.WriteLine("Number of triangles: {0}", validTriangles.Count());

            var newRows = Enumerable
                .Range(0, input.Count() / 3)
                .Select(i => new List<List<int>>{
                    new List<int>{ rows[3*i][0], rows[3*i+1][0], rows[3*i+2][0] },
                    new List<int>{ rows[3*i][1], rows[3*i+1][1], rows[3*i+2][1] },
                    new List<int>{ rows[3*i][2], rows[3*i+1][2], rows[3*i+2][2] }
                }).SelectMany(listTriple => listTriple);
            var validTriangles2 = newRows.Where(row => row.IsTriangle());
            Console.WriteLine("Number of triangles: {0}", validTriangles2.Count());
        }
    }

    public static class Day3Extensions
    {
        public static bool IsTriangle(this IEnumerable<int> source)
        {
            var numbers = source.ToList();
            return numbers[0] + numbers[1] > numbers[2] &&
                   numbers[1] + numbers[2] > numbers[0] &&
                   numbers[0] + numbers[2] > numbers[1];
        }
    }
}