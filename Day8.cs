using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day8
    {
        public static void Run()
        {
            // Part 1
            var screen = new Screen(50, 6);
            var instructions = Misc.ReadInput(day: 8);
            DoInstructions(screen, instructions);
            Console.WriteLine("Lit pixels: {0}", screen.NumberOfLitPixels());

            // Part 2
            screen.Print(5);
        }

        private static void DoInstructions(Screen screen, IEnumerable<string> instructions)
        {

            foreach (var row in instructions)
            {
                var words = row.Split(' ');
                var command = words[0];
                switch (command)
                {
                    case "rect":
                        var size = words[1].Split('x').Select(i => int.Parse(i)).ToArray();
                        screen.Rectangle(size[0], size[1]);
                        break;
                    case "rotate":
                        var dimension = words[1];
                        var offset = int.Parse(words[2].Split('=')[1]);
                        var steps = int.Parse(words[4]);
                        if (dimension == "row")
                            screen.RotateRow(offset, steps);
                        else if (dimension == "column")
                            screen.RotateColumn(offset, steps);
                        break;
                }
            }
        }
    }

    public class Screen
    {
        public Screen(int width, int height)
        {
            Pixels = new bool[height, width];
        }

        public bool[,] Pixels { get; private set; }

        public void Rectangle(int width, int height)
        {
            foreach (var x in Enumerable.Range(0, width))
            {
                foreach (var y in Enumerable.Range(0, height))
                {
                    Pixels[y, x] = true;
                }
            }
        }

        public void RotateRow(int y, int steps)
        {
            var rowValues = RowValues(y).ToList();
            for (var x = 0; x < Pixels.GetLength(1); ++x)
            {
                Pixels[y, (x + steps) % Pixels.GetLength(1)] = rowValues[x];
            }
        }

        public void RotateColumn(int x, int steps)
        {
            var columnValues = ColumnValues(x).ToList();
            for (var y = 0; y < Pixels.GetLength(0); ++y)
            {
                Pixels[(y + steps) % Pixels.GetLength(0), x] = columnValues[y];
            }
        }

        public int NumberOfLitPixels()
        {
            var count = 0;
            for (var x = 0; x < Pixels.GetLength(1); ++x)
            {
                for (var y = 0; y < Pixels.GetLength(0); ++y)
                {
                    if (Pixels[y, x])
                        ++count;
                }
            }
            return count;
        }

        public void Print(int characterWidth)
        {
            for (var y = 0; y < Pixels.GetLength(0); ++y)
            {
                for (var x = 0; x < Pixels.GetLength(1); ++x)
                {
                    if (x % characterWidth == 0 && x != 0)
                        Console.Write("   ");
                    Console.Write(Pixels[y, x] ? "#" : " ");
                }
                Console.WriteLine();
            }
        }

        private IEnumerable<bool> RowValues(int y)
        {
            return Enumerable.Range(0, Pixels.GetLength(1)).Select(x => Pixels[y, x]);
        }

        private IEnumerable<bool> ColumnValues(int x)
        {
            return Enumerable.Range(0, Pixels.GetLength(0)).Select(y => Pixels[y, x]);
        }
    }
}