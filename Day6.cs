using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
    public class Day6
    {
        public static void Run()
        {
            var input = Misc.ReadInput(day: 6).ToList();
            var counts = Enumerable.Range(0, input[0].Length).Select(i => new Count<char>()).ToList();
            foreach (var row in input)
            {
                foreach (var column in Enumerable.Range(0, input[0].Length))
                {
                    counts[column].Add(row[column]);
                }
            }
            var password = string.Join("", counts.Select(count => count.Max()));
            Console.WriteLine("Password: {0}", password);

            var secondPassword = string.Join("", counts.Select(count => count.Min()));
            Console.WriteLine("Password: {0}", secondPassword);
        }
    }

    public class Count<E>
    {
        private Dictionary<E, int> countedElements = new Dictionary<E, int>();

        public void Add(E element)
        {
            if (!countedElements.ContainsKey(element))
                countedElements[element] = 0;
            countedElements[element] += 1;
        }

        public E Max()
        {
            int largestCount = 0;
            E element = default(E);
            foreach (var kv in countedElements)
            {
                if (kv.Value > largestCount)
                {
                    largestCount = kv.Value;
                    element = kv.Key;
                }
            }
            return element;
        }

        public E Min()
        {
            int smallestCount = int.MaxValue;
            E element = default(E);
            foreach (var kv in countedElements)
            {
                if (kv.Value < smallestCount)
                {
                    smallestCount = kv.Value;
                    element = kv.Key;
                }
            }
            return element;
        }
    }
}